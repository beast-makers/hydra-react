import React from "react";
import ReactDOM from "react-dom";
import App from "./Counters/App.jsx";
import "./Counters/App.css";

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
