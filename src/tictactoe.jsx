import React from "react";
import ReactDOM from "react-dom";
import Game from "./TicTacToe/Game.jsx";
import "./TicTacToe/App.css";

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);
