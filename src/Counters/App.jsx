import React from "react";
import Counters from "./Counters.jsx";
import Navbar from "./Navbar.jsx";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counters: [
        { id: 1, value: 4 },
        { id: 2, value: 0 },
        { id: 3, value: 0 },
        { id: 4, value: 0 }
      ]
    };

    this.incrementValue = this.incrementValue.bind(this);
    this.deleteCounter = this.deleteCounter.bind(this);
    this.resetCounters = this.resetCounters.bind(this);
    this.getTotalNonZeroCounters = this.getTotalNonZeroCounters.bind(this);
  }

  incrementValue (counter) {
    const index = this.state.counters.indexOf(counter);
    const counters = [...this.state.counters];
    counters[index] = {...counter};
    counters[index].value++;

    this.setState({
      counters: counters
    });
  }

  deleteCounter (counter) {
    const counters = this.state.counters.filter(function (stateCounter) {
      return stateCounter.id !== counter.id
    });

    this.setState({
      counters: counters
    });
  }

  resetCounters () {
    let counters = [...this.state.counters];
    counters = counters.map(function (counter) {
      counter.value = 0;

      return counter;
    });

    this.setState({
      counters: counters
    });
  }

  getTotalNonZeroCounters () {
    const counters = this.state.counters.filter(function (counter) {
      return counter.value > 0;
    });

    return counters.length;
  }

  render () {
    return (
      <React.Fragment>
      <Navbar totalNonZeroCounters={this.getTotalNonZeroCounters()} />
      <Counters
        counters={this.state.counters}
        onIncrement={this.incrementValue}
        onDelete={this.deleteCounter}
        onReset={this.resetCounters}
      />
      </React.Fragment>
    );
  }
}

export default App;
