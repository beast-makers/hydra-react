import React from "react";

class Counter extends React.Component {
  constructor(props) {
    super(props);
  }

  getValueClassName () {
    const className = 'badge m-small';

    if (this.props.counter.value < 1) {
      return className + ' badge-yellow';
    }

    return className + ' badge-blue';
  }

  formatValue () {
    return this.props.counter.value ? this.props.counter.value : 'zero';
  }

  render () {
    return (
        <div className="item-row">
          <div className={this.getValueClassName()}>{this.formatValue()}</div>
          <div className="btn btn-primary m-small" onClick={() => this.props.onIncrement(this.props.counter)}>+</div>
          <div className="btn btn-secondary m-small" onClick={() => this.props.onDelete(this.props.counter)}>Delete</div>
        </div>
      );
  }
}


export default Counter;
