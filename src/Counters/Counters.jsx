import React from "react";
import Counter from "./Counter.jsx";

export default function Counters ({counters, onIncrement, onDelete, onReset}) {
  return (
    <React.Fragment>
    <div className="btn btn-secondary" onClick={onReset}>Reset</div>
    <div>{counters.map(counter =>
      <Counter
        key={counter.id}
        counter={counter}
        onIncrement={onIncrement}
        onDelete={onDelete}
      />
    )}
    </div>
    </React.Fragment>
  );
}
