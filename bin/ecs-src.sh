#!/usr/bin/env bash
set -euxo pipefail

exec vendor/bin/ecs check src $@
