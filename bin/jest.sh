#!/usr/bin/env bash
set -euxo pipefail

exec node_modules/jest/bin/jest.js
