#!/usr/bin/env bash
set -euxo pipefail

exec vendor/bin/codecept run -c tests/codeception.yml
