#!/usr/bin/env bash
set -euxo pipefail

exec node_modules/eslint/bin/eslint.js src-presentation/js
