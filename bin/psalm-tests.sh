#!/usr/bin/env bash
set -euxo pipefail

exec vendor/bin/psalm tests
